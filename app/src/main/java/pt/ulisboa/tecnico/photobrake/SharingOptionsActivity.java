package pt.ulisboa.tecnico.photobrake;

import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakePicture;
import pt.ulisboa.tecnico.photobrake.engine.PictureOptions;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

public class SharingOptionsActivity extends Activity {

	private PhotoBrakePicture picture;
	private String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sharing_options);
		picture = (PhotoBrakePicture) getIntent().getSerializableExtra("picture");
		username = getIntent().getExtras().getString("username");
		Spinner spinner = (Spinner) findViewById(R.id.deletetimespinner);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.delete_time, android.R.layout.simple_spinner_item);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.next, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_next:
			next();
			return super.onOptionsItemSelected(item);
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	// Bottom menu actions
	public void goToFeed(View view) {
		Intent intent = new Intent(SharingOptionsActivity.this, FeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUpload(View view) {
		Intent intent = new Intent(SharingOptionsActivity.this, UploadActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUsers(View view) {
		Intent intent = new Intent(SharingOptionsActivity.this, UsersActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToPhotos(View view) {
		Intent intent = new Intent(SharingOptionsActivity.this, ManagePicturesListActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void next() {

		PictureOptions options = new PictureOptions();

		if(((CheckBox) findViewById(R.id.printscreencheckbox)).isChecked()) {
			options.setPrintScreen(true);
		} else {
			options.setPrintScreen(false);
		}

		if(((CheckBox) findViewById(R.id.savecheckbox)).isChecked()) {
			options.setSave(true);
		} else {
			options.setSave(false);
		}
		
		if(((CheckBox) findViewById(R.id.sharecheckbox)).isChecked()) {
			options.setShare(true);
		} else {
			options.setShare(false);
		}

		if(((CheckBox) findViewById(R.id.deletecheckbox)).isChecked()) {
			Spinner spinner = (Spinner)findViewById(R.id.deletetimespinner);
			String valToSet = spinner.getSelectedItem().toString();
			
			options.setDelete(true);
			options.setDeleteSeconds(Integer.parseInt(valToSet));
		} else {
			options.setDelete(false);
			options.setDeleteSeconds(0);
		}
		
		picture.setOptions(options);

		Intent intent = new Intent(SharingOptionsActivity.this,
				ShareActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		intent.putExtra("picture", picture);
		startActivity(intent);
	}
}
