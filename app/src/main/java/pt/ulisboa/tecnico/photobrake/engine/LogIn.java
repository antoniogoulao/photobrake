package pt.ulisboa.tecnico.photobrake.engine;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import pt.ulisboa.tecnico.photobrake.comm.ConnectToServer;

public class LogIn {
	private Socket sendSocket = null;
	private String username = null;
	private String password = null;
	private ObjectOutputStream out = null;
	private DataInputStream in = null;
	private LogInState state = null;

	public LogIn(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public LogInState attemptLogin() {
		sendSocket = ConnectToServer.getInstance().connect();

		try {
			out = new ObjectOutputStream(sendSocket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		// TODO cipher password

		try {
			out.writeInt(ActionType.LOGIN.ordinal());
			
			// TODO Fix this
			out.writeUTF(username+"0x1b"+password);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			in = new DataInputStream(sendSocket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			int answer = in.readInt();
			state = LogInState.values()[answer];
		} catch (IOException e) {
			e.printStackTrace();
		}

		return state;
	}

}
