package pt.ulisboa.tecnico.photobrake.engine;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

public class PictureInfoAdapter extends ArrayAdapter<PhotoBrakePicture> {

	Context context;
    int textViewResourceId;
    TableLayout pictureList;
    ArrayList<PhotoBrakePicture> pictures = null;

	public PictureInfoAdapter(Context context, int textViewResourceId,
			ArrayList<PhotoBrakePicture> pictures) {
		super(context, textViewResourceId, pictures);
		this.context = context;
		this.textViewResourceId = textViewResourceId;
		this.pictures = pictures;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		if(row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(textViewResourceId, parent, false);
            
            pictureList = (TableLayout) row.findViewById(R.id.pictureInfoItem);

            PhotoBrakePicture picture = pictures.get(position);
            ByteArrayInputStream imageStream = new ByteArrayInputStream(picture.getData());
            Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
            
            ImageView img = new ImageView(context);
            img.setLayoutParams(new LayoutParams(100, 100));
            img.setImageBitmap(bitmap);
//            pictureList.addView(img);
//            
//            ListView options = new ListView(context);
//            TextView name = new TextView(context);
//            name.setText(picture.getDescription());
//            options.addView(name);
//            
//            if(picture.getOptions().isPrintScreen()) {
//            	TextView prtsc = new TextView(context);
//                name.setText("Print Screen");
//            	options.addView(prtsc);
//            }
//            
//            if(picture.getOptions().isDelete()) {
//            	TextView del = new TextView(context);
//                name.setText("Delete");
//            	options.addView(del);
//            }
//            
//            if(picture.getOptions().isPrintScreen()) {
//            	TextView save = new TextView(context);
//                name.setText("Save");
//            	options.addView(save);
//            }
            
		}
		
		return row;
	}
}
