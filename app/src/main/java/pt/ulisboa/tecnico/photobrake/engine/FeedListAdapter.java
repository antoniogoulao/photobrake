package pt.ulisboa.tecnico.photobrake.engine;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FeedListAdapter extends ArrayAdapter<PhotoBrakeUser> {

	Context context;
    int textViewResourceId;
    LinearLayout linearMain;
    ArrayList<PhotoBrakeUser> friends = null;

	public FeedListAdapter(Context context, int textViewResourceId,
			ArrayList<PhotoBrakeUser> friends) {
		super(context, textViewResourceId, friends);
		this.context = context;
		this.textViewResourceId = textViewResourceId;
		this.friends = friends;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View row = convertView;
		
		if(row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(textViewResourceId, parent, false);
            
            linearMain = (LinearLayout) row.findViewById(R.id.linearMain);

            PhotoBrakeUser friend = friends.get(position);
            
            TextView label = new TextView(context);
            label.setText(friend.getUsername());
            linearMain.addView(label);
		}
		
		return row;
		
	}

}
