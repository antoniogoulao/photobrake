package pt.ulisboa.tecnico.photobrake.engine;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.codehaus.jackson.map.ObjectMapper;

import pt.ulisboa.tecnico.photobrake.comm.ConnectToServer;

public class UploadPicture {

	private Socket sendSocket = null;
	private PhotoBrakePicture picture;
	private String username;
	private ObjectOutputStream out = null;
	private DataInputStream in = null;
	private boolean answer = false;


	public UploadPicture(String username, PhotoBrakePicture picture) {
		this.picture = picture;
		this.username = username;
	}


	public Boolean upload() {
		sendSocket = ConnectToServer.getInstance().connect();

		try {
			out = new ObjectOutputStream(sendSocket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			out.writeInt(ActionType.UPLOAD.ordinal());
			out.writeUTF(username);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(out, picture);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			sendSocket.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//		sendSocket = ConnectToServer.getInstance().connect();
		//		try {
		//			in = new DataInputStream(sendSocket.getInputStream());
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
		//
		//		try {
		//			answer = in.readBoolean();
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
		//
		//		return answer;

		return true;
	}

}
