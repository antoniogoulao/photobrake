package pt.ulisboa.tecnico.photobrake;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.engine.FetchPhotoStream;
import pt.ulisboa.tecnico.photobrake.engine.ManagePictureItemClickListener;
import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakePicture;
import pt.ulisboa.tecnico.photobrake.engine.PhotoStreamItemClickListener;
import pt.ulisboa.tecnico.photobrake.engine.PictureInfoAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class ManagePicturesListActivity extends Activity {

	private String username;
	private FetchPicturesTask fetchPicturesTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_pictures_list);
		username = getIntent().getExtras().getString("username");
		fetchPicturesTask = new FetchPicturesTask(this, username);
		fetchPicturesTask.execute((Void) null);
	}

	// Bottom menu actions
	public void goToFeed(View view) {
		Intent intent = new Intent(ManagePicturesListActivity.this, FeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUpload(View view) {
		Intent intent = new Intent(ManagePicturesListActivity.this, UploadActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUsers(View view) {
		Intent intent = new Intent(ManagePicturesListActivity.this, UsersActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToPhotos(View view) {

	}
	
public class FetchPicturesTask extends AsyncTask<Void, Void, ArrayList<PhotoBrakePicture>> {
		
		private String username;
		private	FetchPhotoStream fetch;
		private Context context;
		private PictureInfoAdapter adapter;
		private ListView dataList;
		
		public FetchPicturesTask(Context context, String username) {
			this.context = context;
			this.username = username;
		}

		@Override
		protected ArrayList<PhotoBrakePicture> doInBackground(Void... params) {
			fetch = new FetchPhotoStream(username);
			return fetch.fetch();
		}
		
		@Override
		protected void onPostExecute(final ArrayList<PhotoBrakePicture> success) {
			adapter = new PictureInfoAdapter(context, R.layout.picture_info_item, success);
	        dataList = (ListView) findViewById(R.id.picturesListView);
	        dataList.setAdapter(adapter);
	        
	        dataList.setOnItemClickListener(new ManagePictureItemClickListener(context, dataList, username));
		}
	}
}
