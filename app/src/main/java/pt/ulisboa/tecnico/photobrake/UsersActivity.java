package pt.ulisboa.tecnico.photobrake;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.engine.FetchUserFriends;
import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakeUser;
import pt.ulisboa.tecnico.photobrake.engine.UsersAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class UsersActivity extends Activity {

	private String username;
	private FetchUserFriendsTask fetchUserFriendsTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_users);
		username = getIntent().getExtras().getString("username");
		fetchUserFriendsTask = new FetchUserFriendsTask(this, username);
		fetchUserFriendsTask.execute((Void) null);
	}

	// Bottom menu actions
	public void goToFeed(View view) {
		Intent intent = new Intent(UsersActivity.this, FeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUpload(View view) {
		Intent intent = new Intent(UsersActivity.this, UploadActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUsers(View view) {

	}

	public void goToPhotos(View view) {
		Intent intent = new Intent(UsersActivity.this, ManagePicturesListActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public class FetchUserFriendsTask extends AsyncTask<Void, Void, ArrayList<PhotoBrakeUser>> {

		private String username;
		private Context context;
		private FetchUserFriends fetch;
		private UsersAdapter adapter;
		private ListView usersList;

		public FetchUserFriendsTask(Context context, String username) {
			this.context = context;
			this.username = username;
		}

		@Override
		protected ArrayList<PhotoBrakeUser> doInBackground(Void... params) {
			fetch = new FetchUserFriends(username);
			return fetch.fetch();
		}

		@Override
		protected void onPostExecute(final ArrayList<PhotoBrakeUser> success) {
			adapter = new UsersAdapter(context, R.layout.user_item, success);
			usersList = (ListView) findViewById(R.id.usersListView);
			usersList.setAdapter(adapter);
		}
	}
}
