package pt.ulisboa.tecnico.photobrake.engine;

import pt.ulisboa.tecnico.photobrake.ShowPhotoActivity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class PhotoStreamItemClickListener implements OnItemClickListener {

	private Context context;
	private ListView dataList;
	private String username;

	public PhotoStreamItemClickListener(Context context, ListView dataList, String username) {
		this.context = context;
		this.dataList = dataList;
		this.username = username;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		PhotoBrakePicture picture = (PhotoBrakePicture) dataList.getItemAtPosition(position);

		Intent intent = new Intent(context, ShowPhotoActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("picture", picture);
		intent.putExtra("username", username);
		context.startActivity(intent);
	}

}
