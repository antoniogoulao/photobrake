package pt.ulisboa.tecnico.photobrake.engine;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.R;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class UserFriendsAdapter extends ArrayAdapter<PhotoBrakeUser> {

	private Context context;
	private int textViewResourceId;
	private LinearLayout friendsList;
	private ArrayList<PhotoBrakeUser> friends = null;
	private boolean[] friendSelection;
	

	public UserFriendsAdapter(Context context, int textViewResourceId,
			ArrayList<PhotoBrakeUser> friends) {
		super(context, textViewResourceId, friends);
		this.context = context;
		this.textViewResourceId = textViewResourceId;
		this.friends = friends;
		friendSelection = new boolean[friends.size()];
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if(row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(textViewResourceId, parent, false);

			friendsList = (LinearLayout) row.findViewById(R.id.sharingfrienditem);

			PhotoBrakeUser friend = friends.get(position);
			
			CheckBox checkBox = new CheckBox(context);
			checkBox.setText(friend.getUsername());
			checkBox.setId(position);

			friendsList.addView(checkBox);
			
			checkBox.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					int id = cb.getId();
					
					if (friendSelection[id]){
						cb.setChecked(false);
						friendSelection[id] = false;
					} else {
						cb.setChecked(true);
						friendSelection[id] = true;
					}
					Log.i("UserFriendsAdapter", "CheckBox: " + id + " matches user: " + friends.get(id));
				}
			});
		}

		return row;
	}

}
