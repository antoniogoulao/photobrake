package pt.ulisboa.tecnico.photobrake.engine;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class UsersAdapter extends ArrayAdapter<PhotoBrakeUser> {

	private Context context;
	private int textViewResourceId;
	private TableLayout usersList;
	private ArrayList<PhotoBrakeUser> users = null;
	

	public UsersAdapter(Context context, int textViewResourceId,
			ArrayList<PhotoBrakeUser> users) {
		super(context, textViewResourceId, users);
		this.context = context;
		this.textViewResourceId = textViewResourceId;
		this.users = users;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if(row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(textViewResourceId, parent, false);

			usersList = (TableLayout) row.findViewById(R.id.useritem);
			
			TextView name = new TextView(context);
			name.setText(users.get(position).getUsername());
			
			CheckBox checkbox = new CheckBox(context);
			checkbox.setChecked(true);
			
			TableRow tRow = new TableRow(context);
			tRow.setLayoutParams(new LayoutParams( LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
			tRow.addView(name);
			tRow.addView(checkbox);
			
			usersList.addView(tRow);
		}
		
		return row;
	}
}
