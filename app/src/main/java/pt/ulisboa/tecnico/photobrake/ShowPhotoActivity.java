package pt.ulisboa.tecnico.photobrake;

import java.io.ByteArrayInputStream;

import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakePicture;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ShowPhotoActivity extends Activity {
	
	private PhotoBrakePicture picture;
	private String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		picture = (PhotoBrakePicture) getIntent().getSerializableExtra("picture");
		username = getIntent().getExtras().getString("username");
		setContentView(R.layout.activity_show_photo);
		
		ByteArrayInputStream imageStream = new ByteArrayInputStream(picture.getData());
        Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
		
		ImageView view = (ImageView) getWindow().getDecorView().findViewById(R.id.image);
		
		view.setImageBitmap(bitmap);
	}
	
	// Bottom menu actions
		public void goToFeed(View view) {
			Intent intent = new Intent(ShowPhotoActivity.this, FeedActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}

		public void goToUpload(View view) {
			Intent intent = new Intent(ShowPhotoActivity.this, UploadActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}

		public void goToUsers(View view) {
			Intent intent = new Intent(ShowPhotoActivity.this, UsersActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}

		public void goToPhotos(View view) {
			Intent intent = new Intent(ShowPhotoActivity.this, ManagePicturesListActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}
}
