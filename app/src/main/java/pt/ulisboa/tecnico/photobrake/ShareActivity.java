package pt.ulisboa.tecnico.photobrake;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.engine.FetchUserFriends;
import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakePicture;
import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakeUser;
import pt.ulisboa.tecnico.photobrake.engine.UploadPicture;
import pt.ulisboa.tecnico.photobrake.engine.UserFriendsAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class ShareActivity extends Activity {

	private PhotoBrakePicture picture;
	private String username;
	private FetchUserFriendsTask fetchUserFriendsTask;
	private UploadPictureTask uploadPictureTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);
		picture = (PhotoBrakePicture) getIntent().getSerializableExtra("picture");
		username = getIntent().getExtras().getString("username");

		ByteArrayInputStream imageStream = new ByteArrayInputStream(picture.getData());
		Bitmap bitmap = BitmapFactory.decodeStream(imageStream);

		ImageView v = (ImageView) findViewById(R.id.sharepicture);
		v.setImageBitmap(bitmap);

		fetchUserFriendsTask = new FetchUserFriendsTask(this, username);
		fetchUserFriendsTask.execute((Void) null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.upload, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_upload:
			upload();
			return super.onOptionsItemSelected(item);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Bottom menu actions
	public void goToFeed(View view) {
		Intent intent = new Intent(ShareActivity.this, FeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUpload(View view) {
		Intent intent = new Intent(ShareActivity.this, UploadActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUsers(View view) {
		Intent intent = new Intent(ShareActivity.this, UsersActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToPhotos(View view) {
		Intent intent = new Intent(ShareActivity.this, ManagePicturesListActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void upload() {

		EditText description = (EditText) findViewById(R.id.description);
		picture.setDescription(description.getText().toString());

		uploadPictureTask = new UploadPictureTask(this, username, picture);
		uploadPictureTask.execute((Void) null);
	}

	public class FetchUserFriendsTask extends AsyncTask<Void, Void, ArrayList<PhotoBrakeUser>> {

		private String username;
		private Context context;
		private FetchUserFriends fetch;
		private UserFriendsAdapter adapter;
		private ListView friendsList;

		public FetchUserFriendsTask(Context context, String username) {
			this.context = context;
			this.username = username;
		}

		@Override
		protected ArrayList<PhotoBrakeUser> doInBackground(Void... params) {
			fetch = new FetchUserFriends(username);
			return fetch.fetch();
		}

		@Override
		protected void onPostExecute(final ArrayList<PhotoBrakeUser> success) {
			adapter = new UserFriendsAdapter(context, R.layout.sharing_friend_item, success);
			friendsList = (ListView) findViewById(R.id.friends);
			friendsList.setAdapter(adapter);
		}

	}
	public class UploadPictureTask extends AsyncTask<Void, Void, Boolean> {

		private PhotoBrakePicture picture;
		private String username;
		private Context context;
		private UploadPicture upload;

		public UploadPictureTask(Context context, String username, PhotoBrakePicture picture) {
			this.context = context;
			this.username = username;
			this.picture = picture;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			upload = new UploadPicture(username, picture);
			return upload.upload();
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			if(success) {
				Toast.makeText(context, "Picture uploaded to server", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(ShareActivity.this,
						FeedActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.putExtra("username", username);
				startActivity(intent);
			} else {
				Toast.makeText(context, "Could not upload picture to server", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
