package pt.ulisboa.tecnico.photobrake;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.engine.FeedItemClickListener;
import pt.ulisboa.tecnico.photobrake.engine.FeedListAdapter;
import pt.ulisboa.tecnico.photobrake.engine.FetchUserFriends;
import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakeUser;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class FeedActivity extends Activity {

	private FetchFeedTask fetchFeedTask;
	private String username;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed);
		username = getIntent().getExtras().getString("username");
		fetchFeedTask = new FetchFeedTask(this, username);
		fetchFeedTask.execute((Void) null);
	}

	// Bottom menu actions
	public void goToFeed(View view) {

	}

	public void goToUpload(View view) {
		Intent intent = new Intent(FeedActivity.this, UploadActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToUsers(View view) {
		Intent intent = new Intent(FeedActivity.this, UsersActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}

	public void goToPhotos(View view) {
		Intent intent = new Intent(FeedActivity.this, ManagePicturesListActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("username", username);
		startActivity(intent);
	}


	@Override 
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	public class FetchFeedTask extends AsyncTask<Void, Void, ArrayList<PhotoBrakeUser>> {

		private String username;
		private	FetchUserFriends fetch;
		private FeedListAdapter adapter;
		private Context context;
		private ListView dataList;

		public FetchFeedTask(Context context, String username) {
			this.context = context;
			this.username = username;
		}

		@Override
		protected ArrayList<PhotoBrakeUser> doInBackground(Void... params) {
			fetch = new FetchUserFriends(username);
			return fetch.fetch();
		}

		@Override
		protected void onPostExecute(final ArrayList<PhotoBrakeUser> success) {
            if(success != null) {
                adapter = new FeedListAdapter(context, R.layout.list, success);
                dataList = (ListView) findViewById(R.id.feedListView);
                dataList.setAdapter(adapter);

                dataList.setOnItemClickListener(new FeedItemClickListener(context, dataList, username));
            }
		}
	}
}
