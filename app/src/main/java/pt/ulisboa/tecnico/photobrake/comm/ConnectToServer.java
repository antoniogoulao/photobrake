package pt.ulisboa.tecnico.photobrake.comm;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ConnectToServer extends Thread {

	private static ConnectToServer instance = null;
	private Socket sendSocket = null;
	
	protected ConnectToServer() {
		// DO NOTHING
	}
	
	public static ConnectToServer getInstance() {
		if(instance == null) {
			instance = new ConnectToServer();
		}
		return instance;
	}
	
	public Socket connect() {
		try {
			// TODO Read ip:port from configuration file
			sendSocket = new Socket("192.168.1.4", 4321);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sendSocket;
		}
}
