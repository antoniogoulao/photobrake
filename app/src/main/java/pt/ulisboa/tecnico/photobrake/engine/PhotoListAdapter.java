package pt.ulisboa.tecnico.photobrake.engine;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class PhotoListAdapter extends ArrayAdapter<PhotoBrakePicture> {
	
	Context context;
    int textViewResourceId;
    LinearLayout linearMain;
    ArrayList<PhotoBrakePicture> pictures = null;

	public PhotoListAdapter(Context context, int textViewResourceId,
			ArrayList<PhotoBrakePicture> pictures) {
		super(context, textViewResourceId, pictures);
		this.context = context;
		this.textViewResourceId = textViewResourceId;
		this.pictures = pictures;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		if(row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(textViewResourceId, parent, false);
            
            linearMain = (LinearLayout) row.findViewById(R.id.linearMain);

            PhotoBrakePicture picture = pictures.get(position);
            
            ByteArrayInputStream imageStream = new ByteArrayInputStream(picture.getData());
            Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
            
            ImageView label = new ImageView(context);
            label.setImageBitmap(bitmap);
            linearMain.addView(label);
		}
		
		return row;
	}
}
