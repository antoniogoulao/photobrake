package pt.ulisboa.tecnico.photobrake.engine;

public enum ActionType {
	LOGIN, FETCHFEED, FETCHPHOTOS, UPLOAD
}
