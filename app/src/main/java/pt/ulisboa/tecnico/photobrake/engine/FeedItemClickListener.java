package pt.ulisboa.tecnico.photobrake.engine;

import pt.ulisboa.tecnico.photobrake.UserPhotosActivity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class FeedItemClickListener implements OnItemClickListener {
	
	private Context context;
	private ListView dataList;
	private String username;
	
	public FeedItemClickListener(Context context, ListView dataList, String username) {
		this.context = context;
		this.dataList = dataList;
		this.username = username;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		PhotoBrakeUser friend = (PhotoBrakeUser) dataList.getItemAtPosition(position);
		
		Intent intent = new Intent(context, UserPhotosActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("selectedUsername", friend.getUsername());
		intent.putExtra("username", username);
		context.startActivity(intent);
	}

}
