package pt.ulisboa.tecnico.photobrake.engine;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.util.ArrayList;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import pt.ulisboa.tecnico.photobrake.comm.ConnectToServer;

public class FetchPhotoStream {

	private String username;
	private Socket sendSocket = null;
	private ObjectOutputStream out = null;
	private ObjectInputStream in = null;
	private ArrayList<PhotoBrakePicture> photos = null;

	public FetchPhotoStream(String username) {
		this.username = username;
	}

	public ArrayList<PhotoBrakePicture> fetch() {
		sendSocket = ConnectToServer.getInstance().connect();
		
		try {
			out = new ObjectOutputStream(sendSocket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			out.writeInt(ActionType.FETCHPHOTOS.ordinal());
			out.writeUTF(username);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			in = new ObjectInputStream(sendSocket.getInputStream());
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ObjectMapper mapper = new ObjectMapper();
		
		try {
			photos = mapper.readValue(in, 
					mapper.getTypeFactory().constructCollectionType(ArrayList.class, PhotoBrakePicture.class));
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			sendSocket.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return photos;
	}
}
