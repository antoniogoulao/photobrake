package pt.ulisboa.tecnico.photobrake;

import pt.ulisboa.tecnico.photobrake.engine.LogIn;
import pt.ulisboa.tecnico.photobrake.engine.LogInState;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class PhotoBrakeActivity extends Activity {

	private UserLoginTask loginTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_brake);
	}

	public void logIn(View view) {
		EditText usernameEntry = (EditText) findViewById(R.id.username);
		EditText passwordEntry = (EditText) findViewById(R.id.password);

		String username = usernameEntry.getText().toString();
		String password = passwordEntry.getText().toString();

		loginTask = new UserLoginTask(username, password);
		loginTask.execute((Void) null);

	}

	public class UserLoginTask extends AsyncTask<Void, Void, LogInState> {

		private String username;
		private String password;
		private LogIn connect;

		public UserLoginTask(String username, String password) {
			this.username = username;
			this.password = password;
		}

		@Override
		protected LogInState doInBackground(Void... params) {

			connect = new LogIn(username, password);
			return connect.attemptLogin();
		}

		@Override
		protected void onPostExecute(final LogInState success) {
			loginTask = null;

			switch(success) {
			case NOEXIST:
				// TODO Ask to register
				break;
			case NOTOK:
				// TODO Clean password and repeat
				break;
			case OK:
				Intent intent = new Intent(PhotoBrakeActivity.this,
						FeedActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.putExtra("username", username);
				startActivity(intent);
				Log.i("LogIn", "OOOOOOOOK");
				break;
			default:
				break;

			}
		}

		@Override
		protected void onCancelled() {
			loginTask = null;
		}
	}
}
