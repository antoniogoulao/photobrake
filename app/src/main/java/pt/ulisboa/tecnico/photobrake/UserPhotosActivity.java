package pt.ulisboa.tecnico.photobrake;

import java.util.ArrayList;

import pt.ulisboa.tecnico.photobrake.engine.FetchPhotoStream;
import pt.ulisboa.tecnico.photobrake.engine.PhotoBrakePicture;
import pt.ulisboa.tecnico.photobrake.engine.PhotoListAdapter;
import pt.ulisboa.tecnico.photobrake.engine.PhotoStreamItemClickListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class UserPhotosActivity extends Activity {
	
	private FetchPicturesTask fetchPicturesTask;
	private String selectedUsername;
	private String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_photo);
		selectedUsername = getIntent().getExtras().getString("selectedUsername");
		username = getIntent().getExtras().getString("username");
		fetchPicturesTask = new FetchPicturesTask(this, selectedUsername, username);
		fetchPicturesTask.execute((Void) null);
	}
	
	// Bottom menu actions
		public void goToFeed(View view) {
			Intent intent = new Intent(UserPhotosActivity.this, FeedActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}

		public void goToUpload(View view) {
			Intent intent = new Intent(UserPhotosActivity.this, UploadActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}

		public void goToUsers(View view) {
			Intent intent = new Intent(UserPhotosActivity.this, UsersActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}

		public void goToPhotos(View view) {
			Intent intent = new Intent(UserPhotosActivity.this, ManagePicturesListActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("username", username);
			startActivity(intent);
		}
	
	public class FetchPicturesTask extends AsyncTask<Void, Void, ArrayList<PhotoBrakePicture>> {
		
		private String selectedUsername;
		private String username;
		private	FetchPhotoStream fetch;
		private Context context;
		private PhotoListAdapter adapter;
		private ListView dataList;
		
		public FetchPicturesTask(Context context, String selectedUsername, String username) {
			this.context = context;
			this.selectedUsername = selectedUsername;
			this.username = username;
		}

		@Override
		protected ArrayList<PhotoBrakePicture> doInBackground(Void... params) {
			fetch = new FetchPhotoStream(selectedUsername);
			return fetch.fetch();
		}
		
		@Override
		protected void onPostExecute(final ArrayList<PhotoBrakePicture> success) {
			adapter = new PhotoListAdapter(context, R.layout.list, success);
	        dataList = (ListView) findViewById(R.id.photoListView);
	        dataList.setAdapter(adapter);
	        
	        dataList.setOnItemClickListener(new PhotoStreamItemClickListener(context, dataList, username));
		}
	}
}
